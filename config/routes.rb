Rails.application.routes.draw do
  resources :photos
  root 'photos#index'
  
  get '/get_image/:id', to:'photos#get_image'
  post '/photos/search', to:'photos#search'
end
