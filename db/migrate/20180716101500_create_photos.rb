class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :title
      t.binary :file
      t.date :date

      t.timestamps null: false
    end
  end
end
