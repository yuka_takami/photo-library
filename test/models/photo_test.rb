require 'test_helper'

class PhotoTest < ActiveSupport::TestCase
  test 'tite, date, fileが正しいとき' do
    t = photos(:p_1).title
    assert_not_nil(t)
    d = photos(:p_1).date
    assert_not_nil(d)
   
  end
  
  test 'titleが空のとき、saveされないこと' do
    photo = Photo.new
    assert photo.invalid?
    assert photo.errors.include?(:title)
  end
end
