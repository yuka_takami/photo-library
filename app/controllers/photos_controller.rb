class PhotosController < ApplicationController
  def index
    @photos = Photo.all 
  end
  
  def new
    @photo = Photo.new
  end

  def create
    file = params[:photo][:file].read
    @photo = Photo.new(title: params[:photo][:title], file: file, date: params[:photo][:date])
#    logger.debug(@photo.inspect)
#    logger.debug(params[:photo][:date])
    @photo.save
    redirect_to '/'
  end
  
  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy
    redirect_to '/'
  end

  def show
    @photo = Photo.find(params[:id])
  end
  
  def edit
    @photo = Photo.find(params[:id])
  end
  
  def update
    file =params[:photo][:file].read
    @photo = Photo.find(params[:id])
    @photo.update(title: params[:photo][:title], file: file, date: params[:photo][:date])
    redirect_to '/'
  end
  
  def get_image
    @photo = Photo.find(params[:id])
    send_data @photo.file, disposition: :inline, type: 'image/jpg'
  end
  
  def search
    @photos = Photo.where(title: params[:keyword])
    render "index"
  end
end
